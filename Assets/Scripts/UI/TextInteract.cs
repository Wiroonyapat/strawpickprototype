using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Prototype.Testing
{
    public class TextInteract : MonoBehaviour
    {

        public void ChangeTextString(string textTo)
        {
            GetComponent<TextMeshProUGUI>().text = textTo;
        }
        
        public void AppendTextString(string textTo)
        {
            GetComponent<TextMeshProUGUI>().text += textTo;
        }

        public void ChangeTextValue(object textTo)
        {
            GetComponent<TextMeshProUGUI>().text = System.Convert.ToString(textTo);
        }
        
        public void AppendTextValue(object textTo)
        {
            GetComponent<TextMeshProUGUI>().text += System.Convert.ToString(textTo);
        }
    }
}

