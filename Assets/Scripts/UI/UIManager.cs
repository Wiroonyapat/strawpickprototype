using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototype.Testing
{
    public class UIManager : MonoBehaviour
    {
        public bool StartOn = false;
        public int StartPage = 0;

        public List<GameObject> PageList = new List<GameObject>();

        [SerializeField]
        private GameObject currentPage;


        private void Start()
        {
            if (PageList[StartPage] != null)
            {
                foreach (GameObject x in PageList)
                {
                    if (x != null)
                    {
                        x.SetActive(false);

                    }
                }
                currentPage = PageList[StartPage];
                if (StartOn) currentPage.SetActive(true);
            }
            else
            {
                Debug.LogWarning("No Page Started");
            }

        }

        public void ChangeScene(int changeTo)
        {
            if (currentPage != null) currentPage.SetActive(false);
            if (PageList[changeTo] != null)
            {
                currentPage = PageList[changeTo];
                PageList[changeTo].SetActive(true);
            }
        }

        public void ActivePage(int whichpage)
        {
            if (PageList[whichpage] != null) PageList[whichpage].SetActive(true);
        }

        public void DisactivePage(int whichpage)
        {
            if (PageList[whichpage] != null) PageList[whichpage].SetActive(false);
        }

    }
}

