using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Prototype.Testing
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField]
        private Transform m_PlayerTransform;

        public Vector3 m_CameraDistance;

        [SerializeField]
        private float m_CameraHight = 5;

        private Vector2 m_MouseMovement;

        private Vector3 m_AngleAdjust = new Vector3(0, 0, 1);

        private float m_HorizonAngle = 0;
        private float m_VerticleAngle = 0;

        [SerializeField]
        [Range(0.01f, 5f)]
        private float m_MouseSensi_X = 2;
        
        [SerializeField]
        [Range(0.01f, 5f)]
        private float m_MouseSensi_Y = 2;


        // Start is called before the first frame update
        void Start()
        {
            Cursor.visible = false;            
        }

        // Update is called once per frame
        void Update()
        {
            MouseInputUpdater();
            CalculateDistanceFromTarget();
            UpdateAngleCamera();

            this.transform.forward = Vector3.Normalize(m_PlayerTransform.position + new Vector3(0, m_CameraHight) - this.transform.position);

            if (!Cursor.visible)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        void UpdateAngleCamera()
        {
            Vector3 normalizeAdjust = new Vector3(m_AngleAdjust.x * m_CameraDistance.x,
                (m_AngleAdjust.y * m_CameraDistance.y) + m_CameraHight,
                m_AngleAdjust.z * m_CameraDistance.z);

            this.transform.position = m_PlayerTransform.position + normalizeAdjust;
        }

        void CalculateDistanceFromTarget()
        {
            m_AngleAdjust.x = Mathf.Cos(m_HorizonAngle) * Mathf.Cos(m_VerticleAngle);
            m_AngleAdjust.z = Mathf.Sin(m_HorizonAngle) * Mathf.Cos(m_VerticleAngle);

            m_AngleAdjust.y = Mathf.Sin(m_VerticleAngle);
        }

        static float ms_Deg2Rad = Mathf.PI / 180f;

        void MouseInputUpdater()
        {
            m_MouseMovement.x = Input.GetAxis("Mouse X");
            m_MouseMovement.y = Input.GetAxis("Mouse Y");

            m_HorizonAngle -= m_MouseMovement.x * m_MouseSensi_X * ms_Deg2Rad;

            m_VerticleAngle += m_MouseMovement.y * m_MouseSensi_Y * ms_Deg2Rad;
            m_VerticleAngle = Mathf.Clamp(m_VerticleAngle, -Mathf.PI/2f + 0.00001f, Mathf.PI/2f - 0.00001f);
        }
    }
}

