using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Prototype.Testing
{
    public class Strawberry : MonoBehaviour, IPickUpAble
    {

        public TextInteract myText;

        Vector3 m_pos_origin;
        float m_timeHover;

        [SerializeField]
        [Range(0.1f, 2)]
        private float m_hoverHight = 0.5f;
        
        [SerializeField]
        [Range(0.1f, 10)]
        private float m_hoverSpeed = 3;

        // Start is called before the first frame update
        void Start()
        {
            m_pos_origin = this.transform.position;
            m_timeHover = 0;
            myText.ChangeTextString("Strawberry : " + InventoryBox.ms_Strawberry_Amount);
        }

        private void FixedUpdate()
        {
            m_timeHover += Time.deltaTime * m_hoverSpeed;
            if(m_timeHover >= 2 * Mathf.PI)
            {
                m_timeHover -= 2 * Mathf.PI;
            }
            this.transform.position = m_pos_origin + new Vector3(0, Mathf.Sin(m_timeHover)) * m_hoverHight;
        }

        public void CollidePickup()
        {
            InventoryBox.ms_Strawberry_Amount += 1;
            Destroy(this.gameObject);
            myText.ChangeTextString("Strawberry : " + InventoryBox.ms_Strawberry_Amount);
        }

        public void InteractPickup()
        {

        }

    }
}

