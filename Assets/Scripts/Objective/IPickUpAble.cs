using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IPickUpAble
{
    public void CollidePickup();
    public void InteractPickup();
}
