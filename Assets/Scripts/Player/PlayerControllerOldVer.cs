using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Prototype.Testing
{
    public class PlayerControllerOldVer : MonoBehaviour
    {

        private Rigidbody m_MyRigidbody;

        [SerializeField]
        private GameObject m_MyCamera;

        [SerializeField]
        private float m_acceralation = 2f;

        [SerializeField]
        private float m_Max_speed = 2f;

        // Start is called before the first frame update
        void Start()
        {
            m_MyRigidbody = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 camera_forward = m_MyCamera.transform.forward;
            Vector3 camera_right = m_MyCamera.transform.right;

            if (Input.GetKey(KeyCode.W))
            {
                m_MyRigidbody.velocity += new Vector3(camera_forward.x, 0, camera_forward.z) * m_acceralation;

                CharactorAdjustRotate(in camera_forward); // check after prototype
            }
            if (Input.GetKey(KeyCode.S))
            {
                m_MyRigidbody.velocity -= new Vector3(camera_forward.x, 0, camera_forward.z) * m_acceralation;

                camera_forward = -camera_forward;
                CharactorAdjustRotate(in camera_forward); // check after prototype
                camera_forward = -camera_forward;
            }
            if (Input.GetKey(KeyCode.D))
            {
                m_MyRigidbody.velocity += new Vector3(camera_right.x, 0, camera_right.z) * m_acceralation;

                CharactorAdjustRotate(camera_right); // check after prototype
            }
            if (Input.GetKey(KeyCode.A))
            {
                m_MyRigidbody.velocity -= new Vector3(camera_right.x, 0, camera_right.z) * m_acceralation;

                camera_right = -camera_right;
                CharactorAdjustRotate(in camera_right); // check after prototype
                camera_right = -camera_right;
            }

            Vector3 rigidClamp = Vector3.ClampMagnitude(m_MyRigidbody.velocity, m_Max_speed);

            m_MyRigidbody.velocity = new Vector3(rigidClamp.x, m_MyRigidbody.velocity.y, rigidClamp.z);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_MyRigidbody.velocity += new Vector3(0, 10, 0);
            }

        }

        //move to charactor animation
        void CharactorAdjustRotate(in Vector3 charactor_forwardTo)
        {
            this.transform.forward = new Vector3(charactor_forwardTo.x, 0, charactor_forwardTo.z);
        }
    }
}

