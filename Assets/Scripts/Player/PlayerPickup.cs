using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PlayerPickup : MonoBehaviour
{

    public UnityEvent e_PickupCall;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickable"))
        {
            e_PickupCall.Invoke();
            other.GetComponent<IPickUpAble>().CollidePickup();
            other.GetComponent<IPickUpAble>().InteractPickup();
        }
    }
}
